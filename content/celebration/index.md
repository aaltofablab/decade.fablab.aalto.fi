+++
title = "Celebration"
intro = "The celebration is a chance to meet many people who have participated in the 10-year long journey of Aalto Fablab. It will take place on 18 November between 14:00 and 18:00 in the main lobby of the Aalto Studios building in Otakaari 7. The event will feature diverse stories from and about Aalto Fablab."

[registration]
  url = "https://forms.gle/tFa2y4M2sJ6cEeM28"
+++

The celebration is a chance to meet many people who have participated in the 10-year long journey of Aalto Fablab. It will take place on 18 November between 14:00 and 18:00 in the main lobby of the Aalto Studios building in Otakaari 7. The event will feature diverse stories from and about Aalto Fablab.

- **When**  
  Thursday 18 November  
- **Time**  
  14:00 - 18:00  
- **Venue**  
  Kipsari Studio, Otakaari 7  

## Program

- **14:00 - 14:15**  
  Opening words by Marcus Korhonen
- **14:15 - 14:30**  
  Philip Dean
- **14:30 - 14:45**  
  Juhani Tenhunen
- **14:45 - 15:30**  
  Company partners / open stage
- **15:30 - 16:00**  
  Massimo Menichinelli
- **16:00 - 16:30**  
  Cindy Kohtala
- **16:30 - 16:45**  
  Neil Gershenfeld
- **16:45 - 17:15**  
  Open stage
- **17:15 - 18:00**  
  Elliot Webb and the Electric Motions

{{<button href="https://forms.gle/tFa2y4M2sJ6cEeM28">}}Register here{{</button>}}
