+++
title = "Bootcamp"
intro = "As the first fab lab in Finland, Aalto Fablab is honored to host the first Finnish fab lab network boot camp, the Fablab Suomi Bootcamp 2021. Friday 19 and Saturday 20 November will be filled with activities where the Finnish fab lab network participants will have the opportunity to meet each other."
+++

As the first fab lab in Finland, Aalto Fablab is honored to host the first Finnish fab lab network boot camp, the Fablab Suomi Bootcamp 2021. Friday 19 and Saturday 20 November will be filled with activities where the Finnish fab lab network participants will have the opportunity to meet each other. They will explore the Aalto University campus and share knowledge, skills, and enthusiasm.

- **When**  
  19 and 20 November 2021  
- **Time**  
  10:00 - 18:00 both days  
- **Where**  
  Aalto Fablab  

Detailed schedule is going to be made available to the participants of the bootcamp. Registration required.

{{<button href="https://forms.gle/b66Sd4izMub292AZ6">}}Register here{{</button>}}
