+++
title = "Salvador Hernandez Gazga"
type = "author"

[photo]
  source = "salvador-hernandez-gazga.jpg"
  credit = "Salvador Hernandez Gazga"

[website]
  url = "https://www.linkedin.com/in/salvahdezgazga/"

+++

Mexican designer exploring diverse cultures around the globe through the lens of architecture. I am  interested in understanding the sociological aspects of the built environment and how these affect the human experience. I believe in the power of data manipulation to better understand complex contexts and develop robust concepts & scenarios.

In my working process I rely heavily on physical and digital models, which I consider the ultimate tools to communicate spatial ideas.
