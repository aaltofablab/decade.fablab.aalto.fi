+++
title = "Wan-Ting Hsieh"
type = "author"

[photo]
  source = "wan-ting-hsieh.jpg"
  credit = "Wan-Ting Hsieh"

[website]
  url = "https://fabacademy.wantinghsieh.com/"
+++

Hi, I’m Wan-Ting Hsieh, a third-year New Media master’s student from Taiwan. Before studying in college, I’d always been interested in drawing and done lots of paintings with a wide range of materials since elementary school.

After learning how to create artworks with the latest technology involved during my bachelor’s degree, I started to enjoy doing installations that require calculation and aesthetic design, which is why I joined Fab Academy to explore more machine-based fabrication methods.
