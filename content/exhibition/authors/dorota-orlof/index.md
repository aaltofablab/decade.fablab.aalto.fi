+++
title = "Dorota Orlof"
type = "author"

[photo]
  source = "dorota-orlof.png"
  credit = "Dorota Orlof"

[website]
  url = "https://dorkastrong.com"
+++

My name is Dorota Orlof aka dorka strong and I am a Visual Storyteller, who knows how to build stuff and create good stories. I am focusing on creation of clear, appealing and meaningful visual communicates and experiences. I work between art, design, and education with a main focus on graphic design.

Because of my big interest in geopolitics and information design I am happy to work with infographics and visual journalism, in spectrum from traditional to experimental approach. On the other hand, I see a big importance in creative education, thus I am always very excited to work on designing workshops, including custom materials. For the commissions I work with projects aligned with my values.
