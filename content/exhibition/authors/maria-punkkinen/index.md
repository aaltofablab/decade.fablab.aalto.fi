+++
title = "Maria Punkkinen"
type = "author"

[photo]
  source = "maria-punkkinen_by-noora-karjalainen.jpg"
  credit = "Noora Karjalainen"

[website]
  url = "https://www.mariapunkkinen.com/"
+++

Maria Punkkinen is a ceramic artist based in Helsinki. She is currently finalizing her master studies in Contemporary Design in Aalto University.

Her works are inspired by dark labyrinths of human minds, shadows and light and the perfection of imperfection. She is interested in touch and its meanings. She creates lively surfaces, unique sculptures and objects for everyday use.
