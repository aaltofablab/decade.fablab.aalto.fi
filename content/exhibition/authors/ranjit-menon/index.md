+++
title = "Ranjit Menon"
type = "author"

[photo]
  source = "ranjit-menon_by-leda-vaneva.jpg"
  credit = "Leda Vaneva"

[website]
  url = "https://www.behance.net/menon"
+++

I am a freelance designer who graduated from Sound in New Media MA programme at the Aalto Media Lab. With the help of Fab Academy, I was able to take abstract experimental ideas into hi-fidelity prototyping through intense digital fabrication.

The prototype I made combines my interest in public domain heritage and ancient archives transforming re-mediating themselves into new electronic interactive forms.
