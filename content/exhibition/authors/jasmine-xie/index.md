+++
title = "Jasmine Xie"
type = "author"

[photo]
  source = "jasmine-xie.jpg"
  credit = "Jasmine Xie"

[website]
  url = "https://bluet87.gitlab.io/fab-academy/"
+++

Jasmine is a Helsinki-based artist who enjoys sparking joy by combining elements of her personal interests and distinctive aesthetics with creatively employed technology, primarily in the form of interactive installations, animated costumes, and enhanced wearables.

She is currently completing her Master's degree at Aalto University.
