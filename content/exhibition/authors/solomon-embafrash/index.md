+++
title = "Solomon Embafrash"
type = "author"

[photo]
  source = "solomon-embafrash_by-aalto-studios.jpg"
  credit = "Aalto Studios"

[website]
  url = "https://fabacademy.org/2019/labs/aalto/students/solomon-embafrash/"
+++

I am 38 years old industrial engineer from Helsinki. I have been involved in the fab lab ecosystem for 6 years as workshop master in Aalto University and two years setting up innovation labs in the 7 Emirates of the UAE.

For my Fab Academy final project, I built a low cost  3-axis CNC machine. Designed, fabricated the mechanics & electronics in-house using the resources in our fab lab. The Changeable delivery mechanism and payload makes it easily convertible into other useful machines for a fab lab.
