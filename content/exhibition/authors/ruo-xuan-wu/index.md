+++
title = "Ruo-Xuan Wu"
type = "author"

[photo]
  source = "ruo-xuan-wu.jpg"
  credit = "Ruo-Xuan Wu"

[website]
  url = "https://www.russianwu.com/"
+++

Russian Wu (Ruo-Xuan) is a master's student based at Bauhaus and Aalto University. His project majors in Digital Fabrication, Interactive Design, and Media Architecture fields. He is the winner of the Media Architecture Biennale 2020 Student Award with the project, Social Painting, which tries to prevent discrimination by interactive projection.

Now he is dedicated to realizing haptic sense to deliver the hug digitally.
