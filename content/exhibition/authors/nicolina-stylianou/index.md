+++
title = "Nicolina Stylianou"
type = "author"

[photo]
  source = "nicolina-stylianou.jpg"
  credit = "n/a"

[website]
  url = "https://www.nicolinastylianou.com/"
+++

I am an artist who designs and fabricates body-sculptures that produce sounds, turns objects into vital things by re-situating the mythical within contemporary settings, and performs with them live. The body-sculptures, is an artistic hybrid of multiple disciplines including: autobiography, philosophy, mythology, and history.

My conceptual thought negotiates the state where objects are not yet identified, and how objects breed perception.
