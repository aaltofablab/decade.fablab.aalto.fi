+++
title = "Anssi Alhopuro"
type = "author"

[photo]
  source = "anssi-alhopuro.jpg"
  credit = "Anssi Alhopuro"

[website]
  url = "http://www.paetkaa.com/"
+++

Anssi Alhopuro is an audiovisual artist and multi-instrumentalist, who has previously worked as a producer, audio engineer and VJ.

While Anssi has always been interested in inventing and building things, learning digital fabrication has provided him with the missing link between his love for digital media and creating things in the physical world.
