+++
title = "Krisjanis Rijnieks"
type = "author"

[text]
  credit = "artybollocks.com"

[photo]
  source = "krisjanis-rijnieks_by-dorota-orlof.jpg"
  credit = "Dorota Orlof"

[website]
  url = "https://rijnieks.com"
+++

Curator of the exhibition. Currently employed at the Aalto Fablab as a lab technician and Fab Academy instructor. He holds an MA from the Aalto Media Lab and has background in digital fabrication, creative coding and projection mapping.

His work explores the relationship between acquired synesthesia and multimedia experiences. With influences as diverse as Richard Stallman and Zachary Lieberman, new variations are synthesised from both orderly and random discourse.
