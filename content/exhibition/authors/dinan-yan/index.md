+++
title = "Dinan Yan"
type = "author"

[photo]
  source = "dinan-yan.jpg"
  credit = "Dinan Yan"

[website]
  url = "https://dinany.gitlab.io/fab-academy/"
+++

My name is Dinan Yan. I am a 2nd year master student in New Media Design and Production at Aalto University, with a background in finance.

I graduated from the UCLA with a bachelor’s degree in business economics in 2016. I changed my track due to the growing interest in creative coding and interactive art. Recently, I have been learning digital fabrication and web development.
