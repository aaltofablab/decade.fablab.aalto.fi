+++
title = "Leda Vaneva"
type = "author"

[photo]
  source = "leda-vaneva.jpg"
  credit = "Leda Vaneva"

[website]
  url = "https://www.ledavaneva.com/"
+++

Leda Vaneva explores the processes of construction in the observable world, human perceptions and their limitations. In her work she aims at discovering alternative ways of experiencing, at visualising the invisible. Her pieces revolve around themes like materiality, agency /control, the connection of the digital and physical realm.

Leda holds MA degrees in New Media (Aalto University) and Photography (National Academy of Art, Soﬁa, BG). Currently she lives and works in Helsinki.
