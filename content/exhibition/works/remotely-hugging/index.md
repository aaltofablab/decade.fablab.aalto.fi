+++
title = "Remotely Hugging - Inflatable Suit"
authors = ["ruo-xuan-wu"]
type = "works"
imcopy = "Ruo-Xuan Wu"
+++

In the past two years, human beings face an unprecedented challenge, the Corona pandemic. All of us strive to protect our loves and others by decreasing direct contact. We greet without hugging, limit traveling, and also avoid visiting our grandparents. Thanks to the internet, we could see and hear them through Facetime or Skype, but the hug and warmth could not be transmitted. I missed them so much that I created the Inflatable Suit to hug them remotely.

Inflatable Suit is a product that enables people to touch and embraces from miles away. Instead of cold and flat screens, Inflatable Suit brings us well-being, while your loves have to leave because of working, studying, or other necessary purposes. This prototype concentrates on the haptic sense of a hug. In the later development, temperature and smell would be introduced to create a stronger memory and emotional connection.
