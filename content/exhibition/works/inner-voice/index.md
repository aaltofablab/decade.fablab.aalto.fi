+++
title = "Inner Voice: Wireless Interactive Wearable Technology"
authors = ["wan-ting-hsieh"]
type = "works"
imcopy = "Wan-Ting Hsieh"
+++

Sometimes, it is embarrassing or difficult to talk about personal feelings through plain words during a conversation. Is there any way to inform others about self discomfort without offending them? According to this, I came up with the idea that is it possible to express ourselves through light controlled by our gestures? For example, by defining certain hand gestures and matching them to specific light patterns, people can understand our current thinking, stop bothering us, and leave us alone to get better.

“Inner Voice” is an e-textile project consisting of a glove connected to a bracelet as a signal sender, eight pieces of light modules in tetrahedron shapes installed on a dress, and a belt as a signal receiver. With it, we can express self-conscious emotions more politely without ruining others’ moods, whether in a party or a public space, simply by using different kinds of hand gestures to light up various patterns remotely.
