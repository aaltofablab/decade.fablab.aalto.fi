+++
title = "Chorus: Roller-Skate Midi Controller"
authors = ["nicolina-stylianou"]
type = "works"
imcopy = "Nicolina Stylianou"
+++

“Chorus” is an interactive MIDI - Musical Instrument Digital Interface roller-skate controller. The MIDI controller processes the movement, the pressure, and the speed of the roller-skates and allow them communicate with digital musical software (i.e., Ableton Live).

The messages sent from the roller-skates to the digital musical software pass through another software that reads the nine sensors attached to the roller- skates hardware, processes the readings, and transmits them to a host for further processing. The sensors’ data is transmitted and read by a host computer software, which produces sounds that further react to the variables read by the sensors. The data and software allow the instrument to create interactive musical compositions with the movements (including pressure, speed, direction) of the performer.

Conceptualisation Nicolina Stylianou. Software development by Joaquín Aldunate. Roller-skate body design in collaboration with George Themistokleous. Artwork courtesy the artist. The work is supported by Kone Foundation, Theatre - Academy of University of the Arts Helsinki, and Taiteen edistämiskeskus (Taike). Special thanks to Krisjanis Rijnieks, Antonis Antoniou, Giorgos Christophorou, Jouni-Ilari Tapio, Karoliina Loimaala, Andreou Laser Brothers Ltd, Prof Dr Tero Nauha, Constantinos Miltiadis, Maria Georgiou, Louna-Tuuli Luukka, Dionysis Voniatis, Kyriakos Fyrillas, and my family.
