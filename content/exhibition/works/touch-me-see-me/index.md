+++
title = "Touch Me - See Me"
authors = ["maria-punkkinen"]
type = "works"
imcopy = "Maria Punkkinen"
+++

Touch is a fundamental part of life. It keeps us alive, comforts, brings joy and makes us human. The work was born out of the conversations about loneliness and a feeling of being an outsider. In this technology driven world it is important to remember that not everything is possible to experience with remote access: we still need each other. The work strives to remind what is important. It asks to stop for a moment, look closer and be present.

The work was made by 3D scanning a human body which was then CNC milled from foam and then turned into a mold and casted from porcelain. All the unintentional mistakes that happened during the process for example during 3D scanning or marks from the milling are essential parts of the surface. Inside the sculpture there is a heater making the sculpture warm. The work is meant to be touched. It is currently exhibited at EMMA museum.
