+++
title = "What’s My Better Profile"
authors = ["leda-vaneva"]
type = "works"
imcopy = "Leda Vaneva"
+++

The piece is part of the Leda Vaneva’s ongoing work, which maps the idea of the digital self and its possible translations into the physical.

What’s my better profile? is reflecting some of the artist’s personal medical data and extrapolating it into a sculptural object.
