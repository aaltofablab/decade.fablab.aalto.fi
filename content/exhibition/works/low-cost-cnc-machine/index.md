+++
title = "Low Cost CNC Machine"
authors = ["solomon-embafrash"]
type = "works"
imcopy = "Aalto Studios"
+++

This low cost CNC machine was created as my Fab Academy final project. The design, mechanics and electronics circuit production was completely made in-house using resources available in our Aalto Fablab. Apart from the stepper, DC motors and power supply, most of the remaining parts are freely available from standard hardware store.

The machine is able to mill PCBs, acrylic, wood and aluminum. Some of my design goals for my final project were to use ATmega328 MCU - easily fabricated in-house. Use free or open-source software as a driver. Adaptable for different work areas, from small PCB mill to mid-sized router. Sufficient accuracy/resolution to mill PCBs for our Fab Academy course. By changing the delivery mechanism and payload, the CNC router can easily be converted into other useful machines for a fab lab.
