+++
title = "Konch"
authors = ["krisjanis-rijnieks", "leda-vaneva", "ranjit-menon", "dorota-orlof"]
type = "works"
imcopy = "Leda Vaneva"
+++

Konch is an open-source multipurpose urban design element that can be made in a standard fab lab. Konch is a multipurpose object enabling discussion or relaxation in the Covid-19 situation. It’s a comfortable seat with an integrated audio system. This way one can keep the social distance recommendations and still have a vivid, organic, screen-less conversation, as a balance to the recently so popular video chats.

Konches can be easily arranged in any constellation up to a 100 meters apart, which is the area covered by the integrated router. The platform is suitable for both outdoor, and indoor setting. Because of its weatherproof coating and its shell-like design, it can be a cosy hideout during cloudy and windy days. It’s perfect for students debating from different corners of the school/university, or simply enjoying a chat over a cup of coffee with some fresh air.
