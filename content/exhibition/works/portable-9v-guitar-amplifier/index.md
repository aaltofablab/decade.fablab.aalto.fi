+++
title = "Portable 9V Guitar Amplifier"
authors = ["anssi-alhopuro"]
type = "works"
imcopy = "Anssi Alhopuro"
+++

A simple portable guitar amplifier built around the LM386 amplifier IC, powered by a single rechargeable 9V battery. The circuit design is based on a "Ruby Amp" design and some other random schematics found floating around the internet. Boxes for the amplifier and speaker cabinet were laser-cut, wood dyed, glued together and finished with tung oil.

Clean sounding, surprisingly loud little amp with volume and tone control.
