+++
title = "PONDER Mantra Box"
authors = ["ranjit-menon"]
type = "works"
imcopy = "Ranjit Menon"
+++

Ponder is a product of Fab Academy 2021, an electronic audio box to explore the "inner net". From loopy mantra chants to ambient nature sounds, this box helps one to set a mindful or esoteric ambience.

The box is an example of multiple digital fabrication processes. The outer box was CNC milled from a block of maple wood while most of the electronics including audio were designed, PCB milled and hand soldered. The lazer cutter was used to "engrave" the Goddess image onto the lid - while 3D printed  parts were used for the enclosure.
