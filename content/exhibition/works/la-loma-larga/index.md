+++
title = "La Loma Larga: Msc. Architecture Master Thesis"
authors = ["salvador-hernandez-gazga"]
type = "works"
imcopy = "Salvador Hernandez Gazga"
+++

This Master thesis aimed to study and understand the reasons behind the current situation of segregation within Mexican cities, anchored in their historical, cultural, political and economic scenes, first at a national level and then focusing on the northern industrial city of Monterrey.

A site of interest, ‘La Loma Larga’, served as an example for a scenario of passive confrontation between wealthy neighbourhoods and impoverished areas. Given that the site has not been completely urbanized nor fenced off; this thesis analyzed the forces that came into play in the area and proposed an alternative for its urbanization that did not imply the construction of hard boundaries that increased urban segregation. The design process was carried out through a parametric process in order to handle the level of complexity present in the built environment by harnessing the power of data processing.
