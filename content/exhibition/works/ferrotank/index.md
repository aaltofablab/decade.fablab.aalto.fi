+++
title = "FerroTank"
authors = ["dinan-yan"]
type = "works"
imcopy = "Dinan Yan"
+++

Ferrofluid is a liquid that can be attracted by magnetic field. This project is a ferrofluid display that has electromagnets in the back of the tank and transparent capacitive touch layer in the front. Magnets can be turned on by finger touch and ferrofluid can follow where your finger goes.
