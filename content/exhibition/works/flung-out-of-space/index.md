+++
title = "Flung Out of Space"
authors = ["jasmine-xie"]
type = "works"
imcopy = "Jasmine Xie"
+++

Flung Out of Space (FOS) is a two-part set of wearable accessories designed to increase intimacy between two people by adding an element of synchrony to their biology, without needing the touch or immediate presence of the other party.

It is manifested as a distinctively styled, coordinated set of choker and bracelet, to be worn separately but at the same event. FOS plays with the visible and the invisible, the public and the private, by creating a visual connection between the two wearers as well as a secret physiological bond through a shared language of biofeedback.
