+++
title = "Exhibition"
intro = "The Digital Fabrication Showcase 2021 exhibition will display the work that has been produced in and with Aalto Fablab during the past years. It is going to take place in the main lobby of Väre, which is the new home for all the departments of the School of Arts, Design, and Architecture. The exhibition will last from 15 to 28 November 2021."
+++

The Digital Fabrication Showcase 2021 exhibition will display the work that has been produced in and with Aalto Fablab during the past years. It is going to take place in the main lobby of Väre, which is the new home for all the departments of the School of Arts, Design, and Architecture.

- **When**  
  15 - 27 November 2021  
- **Time**  
  Mon - Thu: 8:00 - 21:00  
  Fri: 8:00 - 20:00  
  Sat: 9:00 - 17:00  
  Sun: Closed  
- **Venue**  
  Väre Main Lobby  
  Otaniementie 14, 02150 Espoo

A lot has been going on at the Aalto Fablab during the past years. Students have been using the space to complete their coursework and thesis projects. Researchers tapped into the possibilities of rapid prototyping to fine-tune their experimental practice. COVID-19 posed new challenges, and collaborations between different stakeholders have been taking place to tackle them. Aalto Fablab has been a ground for practical use of digital fabrication technology. It is time to showcase the outcomes of the different creative processes that have been taking place there.

A fraction of the works in the exhibition are Fab Academy final projects or have been created using skills obtained during the course. An academic overlay for the Fab Academy has been established, giving Aalto University students an option to pursue the global digital fabrication course as part of their studies. You can find more information on the Fab Academy official website.

You can join the Fab Academy academic overlay by entering the following Aalto University courses.

- (DOM-E5155) Digital Fabrication I
- (DOM-E5157) Digital Fabrication III
- (DOM-E5156) Digital Fabrication II
- (DOM-E5158) Digital Fabrication Studio

If you are not a part of the Aalto University, you can join the official Fab Academy. Follow link to registration below and do not forget to choose Aalto Fablab as your node.

{{<button href="https://fabacademy.org/apply/registration.html">}}Join Fab Academy{{</button>}}
