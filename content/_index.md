+++
title = "Welcome!"
intro = "Aalto Fablab celebrates 10 years of innovation, participation, digital manufacturing, technology and creation with an exhibition and a birthday fiesta on 18 of November! Over the last decade, Aalto Fablab has helped people to conceptualize, design, develop and fabricate, with a focus on world-changing open and accessible technologies."
+++
